﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Celsius
{
    class Program
    {
        private float wynik;

        static int silnia(int a)
        {
            if (a == 1)
                return 1;
            else
                return a * silnia(a - 1); 
        }
              
        static void Main(string[] args)
        {
            int option;
            float temperature, temperatureK;
            float a, b, c, j, delta, weight, height, bmi, year;            

            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("0. Exit");
                Console.WriteLine("1. Convert from Celsius deg to Fahrenhiet.");
                Console.WriteLine("2. Calculate delta of quadratic equation");
                Console.WriteLine("3. Calculate your BMI coefficient");
                Console.WriteLine("5. Check if given year is leap - year");
                Console.WriteLine("6. Check if number B is divider of number A");
                Console.WriteLine("7. Enter 7 numbers and check which one is the biggest");
                Console.WriteLine("8. Simple calculator");
                Console.WriteLine("9. Estimate number of roots of a quadratic equation");
                Console.WriteLine("10. Draw funny shapes");
                Console.WriteLine("11. Factorial");
                Console.WriteLine("12. How many numbers you need to add to reach 100");

                option = Convert.ToInt32(Console.ReadLine());
                if (option == 0)
                    break;

                switch (option)
                {
                    case 0:
                        {
                            Console.WriteLine("Hidden case");
                            break;
                        }
                    case 1:
                        {
                            Console.WriteLine("Enter temp in Celsius deg");
                            temperature = Convert.ToInt32(Console.ReadLine());
                            temperatureK = 32 + (9 / 5) * temperature;
                            Console.WriteLine("Temperature " + temperature + " Celsius deg is equal to  " + temperatureK + " Fahrenheit");
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter coefficient a");
                            a = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter coefficient b");
                            b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter coefficient c");
                            c = Convert.ToInt32(Console.ReadLine());

                            delta = (float)Math.Pow(b, 2) - (4 * a * c);
                            Console.WriteLine(" Delta  = " + delta);
                            break;
                        }
                    case 3:
                        {
                            string status = "";
                            Console.WriteLine("Enter your weight");
                            weight = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter your height");
                            height = Convert.ToInt32(Console.ReadLine());
                            height = height / 100;

                            bmi = weight / (float)Math.Pow(height, 2);
                            if (bmi < 18.5f)
                                status = " underweight";
                            else if (bmi >= 18.5f && bmi <= 24.99f)
                                status = " you're in good shape";
                            else if (bmi > 25.0f)
                                status = " overweight";
                            Console.WriteLine(" Your BMI : " + bmi + " It means : " + status);

                            break;
                        }
                    case 4:
                        {
                            int x, y = 4;
                            x = (y -= 2);
                            Console.WriteLine(x + "   " + y);
                            x = y++;
                            Console.WriteLine(x + "   " + y);
                            x = y--;
                            Console.WriteLine(x + "   " + y);

                            goto case 0;
                            //break;
                        }
                    case 5:
                        {
                            Console.WriteLine("Enter year:");
                            year = Convert.ToInt32(Console.ReadLine());

                            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
                                Console.WriteLine(year + " IS leap-year");
                            else
                                Console.WriteLine(year + " ISN'T leap-year");

                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("Enter number A : ");
                            a = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter number B : ");
                            b = Convert.ToInt32(Console.ReadLine());

                            if (a % b == 0)
                                Console.WriteLine(" Number " + b + " IS divedier of number " + a);
                            else
                                Console.WriteLine(" Number " + b + " ISN'T divedier of number " + a);
                            break;
                        }
                    case 7:
                        {
                            Console.WriteLine("Enter number : ");
                            a = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter number : ");
                            b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter number : ");
                            c = Convert.ToInt32(Console.ReadLine());

                            float localMax;
                            localMax = Math.Max(a, b);
                            Console.WriteLine(" Max : " + Math.Max(localMax, c));
                            break;
                        }
                    case 8:
                        {
                            string operation;
                            float result = 0;

                            Console.WriteLine("Enter number : ");
                            a = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter number : ");
                            b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("What do you want to do ? ( + - * / )");                            
                            operation = Console.ReadLine();

                            if (operation == "+")
                                result = a + b;
                            else if (operation == "-")
                                result = a - b;
                            else if (operation == "*")
                                result = a * b;
                            else if (operation == "/")
                                result = a / b;
                            else
                            {
                                Console.Write("You did something wrong");
                                break;
                            }
                            Console.WriteLine("Result : " + a + " " + operation + " " + b + " = " + result);
                            break;
                        }
                    case 9:
                        {
                            Console.WriteLine("Enter coefficient a");
                            a = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter coefficient b");
                            b = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter coefficient c");
                            c = Convert.ToInt32(Console.ReadLine());

                            delta = (float)Math.Pow(b, 2) - (4 * a * c);

                            if (delta > 0)
                                Console.WriteLine(" Number of roots : 2");
                            else if (delta == 0)
                                Console.WriteLine(" Number of roots : 1");
                            else if (delta < 0)
                                Console.WriteLine(" Number of roots : 0");
                            else
                                Console.WriteLine(" You did something wrong ");
                            break;
                        }
                    case 10:
                        {
                            Console.WriteLine("Enter number of rows");
                            a = Convert.ToInt32(Console.ReadLine());
                            b = 1;
                            for ( int i = 1; i<= a; i++)
                            {
                                for ( j = 0; j < b; j++)
                                {
                                    Console.Write("*");
                                }
                                Console.WriteLine();
                                b++;
                            }
                            break;
                        }
                    case 11:
                        {
                            Console.WriteLine("Enter number");
                            int k = Convert.ToInt32(Console.ReadLine());
                            int result = silnia(k);
                            Console.WriteLine();
                            Console.WriteLine(k + "!  = "  + result);
                            Console.WriteLine();
                            break;
                        }
                    case 12:
                        {
                            float sum = 0;
                            for(int i = 1; i<1000; i++)
                            {
                                sum += i;
                                Console.WriteLine(sum);
                                if (sum >= 100)
                                {
                                    Console.WriteLine(" You need sum : " + i + " numbers");
                                    break;
                                }
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }
    }
}
